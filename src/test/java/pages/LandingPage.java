package pages;

import org.bouncycastle.util.test.TestRandomBigInteger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class LandingPage extends BasePage {

    private final String URL="https://www.rambler.ru/";

    @FindBy(xpath = "//button[@class=\"rui__2FTrL\"]")
    public WebElement loginBtn;

    @FindBy(xpath = "//span[@class=\"rui__1E3a7\"]")
    public WebElement mailAfterLogin;

    @FindBy(xpath = "//a[@class=\"rui__3ILc74r\"]")
    public WebElement linkToEnter;

    public LandingPage(WebDriver driver){
        super(driver);
        PageFactory.initElements(driver,this);
    }

    public LandingPage open(){
        driver.get(URL);
        return this;
    }

    public AuthorizationPage clickToLogin () throws InterruptedException {
        linkToEnter.click();
        return new AuthorizationPage(driver);
    }

    public String getMailAfterLogin(){
        return mailAfterLogin.getText();
    }

    public void isLoggedIn(String mail){
        assertThat(getMailAfterLogin()).isEqualToIgnoringCase(mail);
    }

}
