package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.Set;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class AuthorizationPage extends BasePage{
    @FindBy(id = "login")
    public WebElement loginInput;
    @FindBy(id = "password")
    public WebElement passwordInput;
    @FindBy(xpath = "//button[@class=\"rui-Button-button rui-Button-type-primary rui-Button-size-medium rui-Button-iconPosition-left rui-Button-block\"]")
    public WebElement enterBtn;
    @FindBy(xpath = "//div[@class='rui-FieldStatus-message']")
    public WebElement failAuthText;
    @FindBy(id = "__next")
    public WebElement loginForm;



    public AuthorizationPage(WebDriver driver) throws InterruptedException {
        super(driver);
        switchToAuthPage(driver);
        switchToIframe();
        PageFactory.initElements(driver, this);
    }

    public void switchToAuthPage(WebDriver driver) throws InterruptedException {
        String parentWindow = driver.getWindowHandle();
        Set<String> handles =  driver.getWindowHandles();
        for(String windowHandle  : handles)
        {
            if(!windowHandle.equals(parentWindow))
            {
                driver.switchTo().window(windowHandle);
            }
        }

    }

    public void switchToIframe() throws InterruptedException {
        waitVisible(driver.findElement(By.xpath("//div[@data-id-frame=\"embed\"]/iframe")),15);
        Thread.sleep(2000);
        driver.switchTo().frame(driver.findElement(By.xpath("//div[@data-id-frame=\"embed\"]/iframe")));
    }

    public LandingPage authorization(String login, String pass) throws InterruptedException {
        loginInput.sendKeys(login);
        passwordInput.sendKeys(pass);
        enterBtn.click();
        Thread.sleep(3000);
        return new LandingPage(driver);
    }

    public AuthorizationPage authorizationFailier(String login, String pass) throws InterruptedException {
        loginInput.sendKeys(login);
        passwordInput.sendKeys(pass);
        enterBtn.click();
        Thread.sleep(3000);
        return this;
    }

    public void isLoggedInFailed(){
        assertThat(failAuthText.getText()).isEqualToIgnoringCase("Неправильная почта или пароль");
    }

    public String clickIconAndSwitchToNewWindow(String iconLabel) throws InterruptedException {
        Set<String> handlesBefore =  driver.getWindowHandles();
        Thread.sleep(3000);
        String icon = "//button[@aria-label='"+iconLabel+"']";
        driver.findElement(By.xpath(icon)).click();
        Set<String> handlesAfter =  driver.getWindowHandles();
        handlesAfter.removeAll(handlesBefore);
        driver.switchTo().window(handlesAfter.stream().findFirst().get());
        return driver.getCurrentUrl();
    }

}
